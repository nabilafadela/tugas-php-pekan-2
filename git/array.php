<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh Soal Array</h1>
    <?php
    
    echo "<h3>Soal Nomor 1</h3>";

    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    $Adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];

    print_r($kids);
    print_r($Adults);


    echo "<h3>Soal Nomor 2</h3>";


    echo "total kids : ". count($kids);
    echo "<ol>";
    echo "<li>".$kids[0]."</li>"; 
    echo "<li>".$kids[1]."</li>";
    echo "<li>".$kids[2]."</li>";
    echo "<li>".$kids[3]."</li>";
    echo "<li>".$kids[4]."</li>";
    echo "<li>".$kids[5]."</li>";  

    echo "</ol>";

    
    echo "total Adults : ". count($Adults);
    echo "<ol>";
    echo "<li>".$Adults[0]."</li>"; 
    echo "<li>".$Adults[1]."</li>";
    echo "<li>".$Adults[2]."</li>";
    echo "<li>".$Adults[3]."</li>";
    echo "<li>".$Adults[4]."</li>";  

    echo "</ol>";

    echo "<h3>Soal Nomor 3</h3>";

    $caststatus = [
        ["Name" => "Will Byers", "Age" => 12, "Aliases" => "Will the Wise", "Status" => "Alive"],
        ["Name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
        ["Name" => "Jim Hopper", "Age" => 43, "Aliases" =>  "Chief Hopper", "Status" => "Deceased"],
        ["Name" => "Eleven", "Age" => 12, "Aliases" =>  "El", "Status" => "Alive"]

    ];

    echo "<pre>";
    print_r($caststatus);
echo "</pre>";


    ?>
</body>
</html>