<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih String PHP</p>
    <?php
    echo "<h3> Soal No 1 </h3>";

    $string = "PHP is never old";
    echo "string = " . $string . "<br>";
    echo "Panjang String = " . strlen($string) . "<br>";
    echo "Jumlah Kata =  " . str_word_count($string) . "<br>";
    
    
    echo "<h3> Soal No 3 </h3>";
    $string2 = "I Love PHP";
    echo "string 2 = " . $string2 . "<br>";
    echo "Kata pertama: " . substr($string2,0,1) . "<br>" ; 
    echo "Kata kedua: " . substr($string2,2,4) . "<br>" ; 
    echo "Kata Ketiga: " . substr($string2,7,3) . "<br>" ;


    echo "<h3> Soal No 3 </h3>";
    $string3 = "PHP is old but sexy!";
    echo "String 3 = " . $string3 . "<br>";
    echo "String 3 = " . str_replace("sexy","awesome",$string3);

    ?>
</body>
</html>